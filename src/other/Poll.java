package other;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.Context;

/**
 * A class that sends a request to get messages from the server.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class Poll {
	private static final String CONTACT = "contact";
	private static final String MESSAGE = "message";

	public ArrayList<String> getAllMessages(String username, String recipient) {
		return poller(username, recipient, true);
	}

	public ArrayList<String> getNewMessages(String username, String recipient) {
		return poller(username, recipient, false);
	}

	private ArrayList<String> poller(String username, String recipient, Boolean getAll) {
		ArrayList<String> messages = new ArrayList<String>();
		String[] query, msgs, elements;
		String answer = "";
		TCPHandlerAndroid tcp = new TCPHandlerAndroid();
		query = new String[3];
		query[0] = username;
		query[1] = recipient;
		if (getAll) {
			query[2] = "@4-8-15-16-23-42";
		}else {
			query[2] = "@4-8-15-16-23-42-LOST";
		}
		tcp.execute(query);
		try {
			answer = tcp.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		msgs = answer.split("%");
		for (int i = 0; i < msgs.length; i++) {
			System.out.println("msgs=" + msgs[i]);
		}

		try {
			for (int i = 0; i < msgs.length; i++) {
				elements = msgs[i].split("#");
				messages.add(elements[0] + " > " + " " + elements[1] + "\n" + elements[2]);
			}

		} catch (Exception e) {
			System.out.println("No new messages");
		}
		return messages;
	}
}