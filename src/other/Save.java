package other;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;


/**
 * A dirty class that will be repaces asap. It handles the saving of contacts in
 * a way that works, but isn't very good. It writes the contact list to a file on the 
 * internal storage, on the device.
 * 
 * There might be some fragments left from when I used this class to save messages too.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 *
 */
public class Save {
	private static final String FILENAME = "Contacts.txflx";
	private static final String CONTACT = "contact";
	private static final String MESSAGE = "message";

	private boolean newMessageFlag;

	private FileInputStream fis = null;
	private FileOutputStream fos = null;
	private Context c;

	/**
	 * 
	 * Uses "#", ie. hash sign to separate saved names.
	 * 
	 * @param c - Context
	 */
	public Save(Context c) {
		this.c = c;
	}

	public ArrayList<String> getAll(String filename) { // Should be "contacts" or username+recipient!
		ArrayList<String> contactlist = new ArrayList<String>();
		long start = System.currentTimeMillis();
		String[] temp;
		StringBuffer fileContent = new StringBuffer("");
		try {
			fis = c.openFileInput(filename);
			byte[] buffer = new byte[1024];
			while (fis.read(buffer) >= 0) {
				fileContent.append(new String(buffer));
			}

			temp = fileContent.toString().split("#");

			for (int i = 0; i < temp.length - 1; i++) {
				contactlist.add(temp[i]);
				System.out.println(temp[i] + "==" + contactlist.get(i));

			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CRASH IN GETALL()");
		}
		System.out.println("getAll() took: " + (System.currentTimeMillis() - start) + "ms");
		return contactlist;
	}

	public void add(String[] content) {
		System.out.println("## Inside Save.add");
		if (content[0].toString().equals(CONTACT)) {
			String type = content[0].toString();
			ArrayList<String> contactlist = getAll(type);
			contactlist.add(content[1]);
			save(contactlist, type);
		} else if (content[0].toString().equals(MESSAGE)) {
			String conversation = (content[1].toString() + content[2].toString());
			System.out.println("### SAVING CONVERSATION AS: " + conversation);
			ArrayList<String> contactlist = getAll(conversation);
			System.out.println("### MESSAGE TO BE SAVED: " + content[3]);
			contactlist.add(content[3]); // TODO: Fill with more information, things like timestamp and such.
			setNewMessageFlag(true);
			save(contactlist, conversation);
		}
	}

	/**
	 * A simple linear search may not seem very optimal here, but after some
	 * tests I concluded that it was not worth the effort to implement a faster
	 * search method right now. Searching 1000 elements takes around 300ms (wich
	 * is much, but not too much).
	 * 
	 * TODO: Implement way of removing messages
	 * 
	 * @param content - [0]type (contact/message) [1]username [2]receiver
	 *            [3]message
	 */

	public void remove(String[] content) {
		System.out.println("## Inside contacts.delete");
		if (content[0].toString().equals(CONTACT)) {
			String type = content[0].toString();
			ArrayList<String> contactlist = getAll(type);
			int foundAtPos = -1;
			// TODO: Gör om till en mer effektiv while loop!
			for (int i = 0; i < contactlist.size(); i++) {
				if (contactlist.equals(contactlist.get(i))) {
					foundAtPos = i;
					contactlist.remove(foundAtPos);
					save(contactlist, type);
				}
			}
		}
	}

	// TODO: Thread it better!
	private void save(ArrayList<String> cl, String filename) {
		System.out.println("## Inside contacts.save");
		String append = null;
		long start;
		System.out.println("length= " + cl.size());
		try {
			System.out.println("Inside try - save");
			start = System.currentTimeMillis();
			System.out.println("innan fos");
			fos = c.openFileOutput(filename, Context.MODE_PRIVATE);
			System.out.println("efter fos");
			System.out.println("length = " + cl.size());
			for (int i = 0; i < cl.size(); i++) {
				append = cl.get(i).toString() + "#";
				fos.write(append.getBytes());
			}
			System.out.println("Saving took " + (System.currentTimeMillis() - start) + "ms");
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getLatestMessage(String filename) {
		System.out.println("LAST MESSAGE KÖRS!");
		String message = getAll(filename).get(getAll(filename).size()-1);
		System.out.println("message="+message);
		setNewMessageFlag(false);
		return message;
	}

	public boolean getNewMessageFlag() {
		return newMessageFlag;
	}

	public void setNewMessageFlag(boolean flag) {
		newMessageFlag = flag;
	}
}
