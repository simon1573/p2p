package other;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import android.os.AsyncTask;

/**
 * A class that handles all networking in the application.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 *
 */

public class TCPHandlerAndroid extends AsyncTask<String, Void, String> {
	private String[] badWords = { "DELETE", "INSERT", "DROP", "SELECT", "UPDATE" };
	private String query;
	private String answer;
	private Socket socket;
	private int port;
	private long systime;
	private boolean connected = false;

	@Override
	protected String doInBackground(String... query) {
		inspect(query);
		connect();
		return transfer();
	}

	/**
	 * Checks the query for words used to manipulate the database, and removes them.
	 * 
	 * @param query - an array with sender, recipient, and message or just username.
	 */
	private void inspect(String[] query) {
		systime = System.currentTimeMillis();

		if (query.length == 1) {
			port = 4948;
			this.query = query[0];
		} else {
			this.query = query[0];
			for (int i = 1; i < query.length; i++) {
				this.query += "," + query[i];
			}
			port = 4949;
		}

		// Check for "bad" input, ie. SQL-injection. I am aware that 
		// it's not in any way the most optimal way of preventing it, 
		// but I think it somehow works!
		long s = System.currentTimeMillis();
		long s1 = System.currentTimeMillis();

		if (this.query.contains("DELETE") || this.query.contains("INSERT") || this.query.contains("DROP") || this.query.contains("SELECT") || this.query.contains("UPDATE")) {
			for (int i = 0; i < badWords.length; i++) {
				s1 = System.currentTimeMillis();
				this.query = this.query.replace(badWords[i], "****");
				s += s1;
			}
		}
	}

	/**
	 * Connects the the socket at server end.
	 */
	private void connect() {
		try {
			socket = new Socket(InetAddress.getByName("tuxflux.se"), port);
			connected = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles the transfer of data to and from the server.
	 * @return a string with the answer from the server.
	 */
	private String transfer() {
		if (connected) {
			try {
				ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
				output.writeUTF(this.query);
				output.flush();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("####################\nTransfer 1 crash\n####################");
			}

			try {
				ObjectInputStream input = new ObjectInputStream(socket.getInputStream());
				answer = input.readUTF();
				System.out.println("Response= " + answer);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("####################\nTransfer 2 crash\n####################");
			}

			try {
				socket.close();
				System.out.println("Query took " + (System.currentTimeMillis() - systime) + "ms to complete");
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("####################\nTransfer 3 crash\n####################");
			}
			return answer.toString();
		} else {
			return "Could not connect to database";
		}
	}
}