package com.tuxflux.p2p;

import java.util.ArrayList;

import other.Poll;
import other.Save;
import other.TCPHandlerAndroid;
import android.R.bool;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 
 * This class represents a conversation activity. Has input for 
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 * @version 1.0
 * 
 */
public class MessageActivity extends Activity {

	private String[] query;
	public static final String PREFS_NAME = "Settings";
	private static final String CONTACT = "contact";
	private static final String MESSAGE = "message";
	private EditText et_Recipient;
	private EditText et_Message;
	private String username, receiver, message;
	private ArrayList<String> messages = new ArrayList<String>();
	private ArrayList<String> newMessages = new ArrayList<String>();
	private ArrayAdapter<String> arrayAdapter;
	private Thread pollingThread;
	private ListView lv;
	private boolean pollingstate;
	private boolean firstPoll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message);
		et_Message = (EditText) findViewById(R.id.et_textInput);
		et_Recipient = (EditText) findViewById(R.id.et_recipient);
		lv = (ListView) findViewById(R.id.lv_messages);

		try {
			receiver = getIntent().getExtras().getString("recipient");
			et_Recipient.setText(receiver);
			setTitle(getString(R.string.chattingWith)+" "+receiver);
			et_Recipient.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}

		SharedPreferences sp = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
		username = sp.getString("username", null);
		arrayAdapter = new ArrayAdapter<String>(getMessageApplicationContext(), android.R.layout.simple_list_item_1	, messages);
		lv.setAdapter(arrayAdapter);
		lv.refreshDrawableState();
		setPollingstate(true);
		setfirstPoll(true);
		startPolling();

		// Send button.
		Button findButton = (Button) findViewById(R.id.button_send);
		findButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				receiver = et_Recipient.getText().toString();
				message = et_Message.getText().toString();
				sendMessage(username, receiver, message);
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		setfirstPoll(true);
		setPollingstate(true);
	}
	@Override
	protected void onRestart() {
		super.onRestart();
		setfirstPoll(true);
		setPollingstate(true);
	}
	
	@Override
	protected void onPause() {
		setPollingstate(false);
		setfirstPoll(false);
		super.onPause();
	}

	@Override
	protected void onStop() {
		setPollingstate(false);
		setfirstPoll(false);
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		setPollingstate(false);
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.message, menu);
		return true;
	}

	/**
	 * Method to be called when user presses the send-button.
	 * 
	 * @param username - the senders username.
	 * @param recipient - the recipients' username.
	 * @param message - message text.
	 */
	private void sendMessage(String username, String recipient, String message) {
		TCPHandlerAndroid tcp = new TCPHandlerAndroid();
		query = new String[3];
		query[0] = username;
		query[1] = recipient;
		query[2] = message;
		tcp.execute(query);
		et_Message.setText("");
	}

	/**
	 * Method with inner class that runs a separate thread, polling for messages
	 * from the message server. It will adjust the polling rate accordning to
	 * the time it takes to get a response from the server.
	 * 
	 * Will write the response to the message-arraylist, then call the method
	 * "updateListview" wich will add the entrys in the arraylist to the
	 * listview.
	 */
	private void startPolling() {
		pollingThread = new Thread() {
			@Override
			public void run() {
				try {
					while (getPollingstate()) {
						Poll poll = new Poll();
						long t = System.currentTimeMillis();
						if (getfirstPoll()) {
							clearNewMessages();
							setNewMessages((poll.getAllMessages(username, receiver)));
							updateListview();
							setfirstPoll(false);
						}
						sleep(((System.currentTimeMillis() - t) * 2)+200); // "flow control" - will make sure that users can't poll faster than 5 polls/s, and not flow to fast if on a bad connection.
						clearNewMessages();
						setNewMessages((poll.getNewMessages(username, receiver)));
						updateListview();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), getString(R.string.pollingfailed), Toast.LENGTH_LONG).show();
				}
			}
		};
		pollingThread.start();
	}

	/**
	 * Method that updates the listview, since other threads are not allowed to
	 * modify it. It will read the newMessages arraylist and add it to the
	 * listview.
	 */
	private void updateListview() {
		runOnUiThread(new Runnable() {
			public void run() {
				ListView lv = (ListView) findViewById(R.id.lv_messages);
				arrayAdapter.addAll(getNewMessages());
				arrayAdapter.notifyDataSetChanged();
				lv.refreshDrawableState();

				// If it's the first time (ie. it's loading all history too) - autoscroll to bottom.
				System.out.println("First run: " + getfirstPoll());
				if (getfirstPoll()) {
					lv.smoothScrollToPosition(arrayAdapter.getCount());
				}
			}
		});
	}

	/**
	 * Method that returns the MessageActvitiy-context.
	 * 
	 * @return context - context of MessageActivity.
	 */
	private Context getMessageApplicationContext() {
		return getApplicationContext();
	}

	/**
	 * Sets the pollingstate flag.
	 * 
	 * @param pollingstate - true to activate polling, false to stop polling.
	 */
	private void setPollingstate(boolean pollingstate) {
		this.pollingstate = pollingstate;
	}

	/**
	 * Returns the pollingstate.
	 * 
	 * @return pollingstate - true if polling is active, false if polling has
	 *         stopped.
	 */
	private boolean getPollingstate() {
		return pollingstate;
	}

	/**
	 * Returns true if it's the first time polling.
	 * 
	 * @return firstPoll - true if it's the first time polling, false if polling
	 *         has been running more than once.
	 */
	private boolean getfirstPoll() {
		return firstPoll;
	}

	/**
	 * Sets the firstPoll flag.
	 */
	private void setfirstPoll(boolean flag) {
		this.firstPoll = flag;
	}

	/**
	 * Returns the newMessages arraylist.
	 * 
	 * @return newMessages - an arraylist used to write new messages in.
	 */
	private ArrayList<String> getNewMessages() {
		return newMessages;
	}

	/**
	 * Writes a new arraylist to the newMessages arraylist.
	 * @param newContent - the arraylist to overwrite the newMessages arraylist.
	 */
	private void setNewMessages(ArrayList<String> newContent) {
		this.newMessages = newContent;
	}

	/**
	 * Method to clear the newMessages arraylist.
	 */
	private void clearNewMessages() {
		this.newMessages.clear();
	}
}