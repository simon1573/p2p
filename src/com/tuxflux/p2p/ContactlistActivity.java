package com.tuxflux.p2p;

import other.Save;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;


/**
 * This class is used to present the contact for the user in a listview. Clicking on
 * a contact will load the conversation between the user and the contact whom is clicked.
 * 
 * When stripping the class from old and redundant code, I broke the longpress thingy. 
 * TODO: Fix the long press thing!
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 *
 */

public class ContactlistActivity extends ListActivity {
	private String target;
	private static final String CONTACT = "contact";
	private static final String MESSAGE = "message";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Save contact = new Save(this);
		ListView lv = getListView();
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact.getAll(CONTACT));
		setListAdapter(arrayAdapter);
		// Courtesy of: http://stackoverflow.com/questions/14340579/android-removing-item-from-listview-on-long-click
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long arg3) {
				setLongClickTarget(getListAdapter().getItem(position).toString());
				popupLongPress();
				return false;
			}
		});
		;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		openConversation(getListAdapter().getItem(position).toString());
	}

	/**
	 * A dialog, showing the option to remove the specific contact 
	 * the user long pressed.
	 */
	public void popupLongPress() {
		String[] choices = new String[] {getString(R.string.remove)};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setItems(choices, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Save contact = new Save(getApplicationContext());
				switch (which) {
				//Remove contact
				case 0:
					String[] content = new String[]{CONTACT, getLongClickTarget()};
					contact.remove(content);
					updateListview();
					break;
				default:
					break;
				}
			}
		});
		builder.show();
	}

	/**
	 * Populating the listview with the contacts
	 * the user has saved.
	 */
	private void updateListview() {
		Save contact = new Save(this);
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact.getAll(CONTACT)));
		getListView().invalidateViews();

	}

	/**
	 * Sets the target to the username.
	 * @param username
	 */
	private void setLongClickTarget(String username) {
		this.target = username;
	}

	/**
	 * Returns the target, the username the user long pressed.
	 * @return username the user long pressed.
	 */
	private String getLongClickTarget() {
		return this.target;
	}

	/**
	 * Starts a new chat with the contact the user pressed.
	 * @param recipient - the receiver in the conversation.
	 */
	private void openConversation(String recipient) {
		System.out.println("Starting conversation with " + recipient);
		Intent newChat = new Intent(this, MessageActivity.class);
		newChat.putExtra("recipient", recipient);
		startActivity(newChat);
	}
}